# Some code copied and/or modified from
# https://utdemir.com/posts/hakyll-on-nixos.html
let
  pkgs = let
    hostPkgs = import <nixpkgs> {};
    pinnedPkgs = hostPkgs.fetchFromGitHub {
      owner = "NixOS";
      repo = "nixpkgs-channels";
      rev = "6a3f5bcb061e1822f50e299f5616a0731636e4e7";
      sha256 = "1ib96has10v5nr6bzf7v8kw7yzww8zanxgw2qi1ll1sbv6kj6zpd";
    };
  in import pinnedPkgs {};

  ghc = pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
    hakyll
    strings
    tagsoup
    uri-encode
  ]);

  padeGenerator = pkgs.stdenv.mkDerivation {
    name = "PadeWebsiteGenerator";
    src = ./generator;
    phases = "unpackPhase buildPhase";
    buildInputs = [ ghc ];
    buildPhase = ''
      mkdir -p $out/bin
      ghc -O2 -dynamic --make site.hs -o $out/bin/generate-site
    '';
  };
in {
  padeTemplate = pkgs.stdenv.mkDerivation {
    name = "PadeWebsiteTemplate";
    src = ./site;
    buildInputs
    = [ ghc
        padeGenerator
        pkgs.compass
        pkgs.rubber
        pkgs.poppler_utils
        pkgs.sass
        pkgs.html-tidy
        pkgs.texlive.combined.scheme-full
      ];
    phases = "unpackPhase buildPhase";
    buildPhase = ''
      export LOCALE_ARCHIVE="${pkgs.glibcLocales}/lib/locale/locale-archive";
      export LANG=en_US.UTF-8
      generate-site build
      mkdir $out
      cp -r _site/* $out
    '';
  };
}
