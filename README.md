# A website to showcase research and teaching, built in Hakyll

## Building

The code is in the `generator` directory. To create the generator
binary, build with stack:

```bash
stack build
```

This creates the executable `site` that can build the website, based on the
templates (and other files) in the `website` directory

To actually build the website, run the executable from the `website`
directory:

```bash
cd website
/path/to/site build
```

This will produce a `_site` directory with the appropriate html, css,
and javascript.

## Requirements

* Texlive
* Rubber (the LaTeX wrapper)
* ruby-sass
