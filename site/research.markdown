---
title: Research
---

<span class="latex">L<sup>a</sup>T<sub>e</sub>X</span> files are
automatically converted to HTML. For serious reading, download the
pdf.

## Computer Science

[RC Crosswords](papers/rc-cross.html)

[Twitter Influence Analysis](https://qfjp-capstone-project.herokuapp.com)

[Classifying Strong PV Numbers](papers/PVNumbers.pdf)

## Physics

![](papers/detector.png)

[Glass Panel Neutron Detector](papers/inglis2012.pdf)
