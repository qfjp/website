---
title: Home
---

<img class="main-img" src="images/qr-code.png" alt="qr-code">
<div class="center">
<a href="DanielPadeResume.pdf">Résumé</a><br>
<a href="DanielPadeCV.pdf">Curriculum Vitæ</a>
</div>
