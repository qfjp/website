---
title: 'Homework 5'
due: 'Due: October 31, 2019'
author: 'Dr. Fenner'
date: '2019-10-01'
time: 1569974440
tocbot: true
layout: 'post'
---

### Reading

* &#167;3.4, and
* &#167;4.1 -- 4.5

### To Submit

#### Written

<dl>
<dt>**Required**</dt>
<dd>
<ul>
<li>[Exercise 4.1.1](#e4.1.1)</li>
<li>[Exercise 4.1.2(a,b,c,d)](#e4.1.2)</li>
<li>[Exercise 4.2.1](#e4.2.1)</li>
<li>[Exercise 4.2.3](#e4.2.3)</li>
<li>[Exercise 4.3.1(a)](#e4.3.1)</li>
<li>[Exercise 4.4.1](#e4.4.1)</li>
<li>[Exercise 4.5.1](#e4.5.1)</li>
</ul>
</dd>

<dt>**Graduate Students Only**</dt>
<dd>
<ul>
<li>[Exercise 4.1.6](#e4.1.6)</li>
<li>[Exercise 4.1.7](#e4.1.7)</li>
<li>[Exercise 4.3.1(c)](#e4.3.1)</li>
</ul>
</dd>
</dl>

#### SQL

Do [Exercise 6.3.1](#e6.3.1) from the textbook, using subqueries as described
there and in class. Submit as you did in the last homework assignment.

---

### Reference

#### <span id="e4.1.1">Exercise 4.1.1</span>

Design a database for a bank, including information about customers
and their accounts. Information about a customer includes their name,
address, phone, and Social Security number. Accounts have numbers,
types (e.g. savings, checking) and balances. Also record the
customer(s) who own an account. Draw the E/R diagram for this
database. Be sure to include arrows where appropriate, to indicate the
multiplicity of a relationship.

#### <span id="e4.1.2">Exercise 4.1.2</span>

Modify your solution to [Exercise 4.1.1](#e4.1.1) as follows:

<ol style="list-style-type: lower-alpha">
<li>Change your diagram so an account can have only one customer</li>
<li>Further change your diagram so a customer can have only one
account.</li>
<li class=difficult>Change your original diagram of [Exercise
4.1.1](#e4.1.1) so that a customer can have a set of addresses (which
are street-city-state triples) and a set of phones. Remember that we
do not allow attributes to have nonprimitive types, such as sets, in
the E/R model.</li>
<li class=difficult>
Further modify your diagram so that customers can have a set of
addresses, and at each address there is a set of phones.
</li>
</ol>

#### <span id="e4.1.6" class=difficult>Exercise 4.1.6</span>

Design a genealogy database with one entity set: *People*. The
information to record about persons includes their name (an
attribute), their mother, father, and children.

#### <span id="e4.1.7" class=difficult>Exercise 4.1.7</span>

Modify your "people" database design of [Exercise 4.1.6](e4.1.6) to
include the following special types of people:

1. Females.
2. Males.
3. People who are parents.

You may wish to distinguish certain other kinds of people as well, so
relationships connect appropriate subclasses of people.

#### <span id="e4.2.1">Exercise 4.2.1</span>

In [Fig. 4.14](#f4.14) is an E/R diagram for a bank database involving customers
and accounts. Since customers may have several accounts, and accounts
may be held jointly by several customers, we associate with each
customer an "account set," and accounts are members of one or more
account sets. Assuming the meaning of the various relationships and
attributes are as expected given their names, criticize the design.
What design rules are violated? Why? What modifications would you
suggest?

<figure id="f4.14">
```tikzpicture
\node[entity] (acctsets) {AcctSets};
\node[attribute] (ownadd) [above=of acctsets] {owner-address} edge (acctsets);

\node[relationship] (memof) [below=of acctsets] {Member of} edge (acctsets);

\node[entity] (acct) [below=of memof] {Accounts} edge (memof);
\node[attribute] (bal) [below=of acct] {balance} edge (acct);
\node[attribute] (num) [right=of acct] {number} edge (acct);

\node[relationship] (has) [right=of acctsets] {Has} edge[-{Stealth[scale=3.0]}] (acctsets);

\node[entity] (cust) [right=of has] {Customers} edge[{Stealth[scale=3.0]}-] (has);
\node[attribute] (name) [above=of cust] {name} edge (cust);

\node[relationship] (livesat) [below=of cust] {Lives at} edge (cust);

\node[entity] (addr) [below=of livesat] {Addresses} edge[{Stealth[scale=3.0]}-] (livesat);
\node[attribute] (addrattr) [below=of addr] {address} edge (addr);
```
<figcaption>Fig. 4.14: A poor design for a bank database</figcaption>
</figure>

#### <span id="e4.2.3">Exercise 4.2.3</span>

Suppose we delete the attribute *address* from *Studios* in [Fig.
4.7](#f4.7). Show how we could then replace an entity set by an
attribute. Where would that attribute appear?

<figure id="f4.7">
```tikzpicture
\node[relationship] (contracts) {Contracts};
\node[attribute] (sal) [above=of contracts] {salary} edge (contracts);

\node[entity] (studios) [below=of contracts] {Studios} edge[{Stealth[scale=3.0]}-] (contracts);
\node[attribute] (sname) [below left=1cm and 0.1cm of studios] {name} edge (studios);
\node[attribute] (saddr) [below right=1cm and 0.1cm of studios] {address} edge (studios);

\node[entity] (stars) [above right=0.5cm and 2.5cm of contracts] {Stars} edge (contracts);
\node[attribute] (stname) [above right=1cm and 0.1cm of stars] {name} edge (stars);
\node[attribute] (staddr) [above left=1cm and 0.1cm of stars] {address} edge (stars);

\node[entity] (movies) [above left=0.5cm and 2.5cm of contracts] {Movies} edge (contracts);
\node[attribute] (title) [above left=1cm and 0.1cm of movies] {title} edge (movies);
\node[attribute] (year) [above right=1cm and 0.1cm of movies] {year} edge (movies);
\node[attribute] (length) [below left=1cm and 0.1cm of movies] {length} edge (movies);
\node[attribute] (genre) [below right=1cm and 0.1cm of movies] {genre} edge (movies);
```
<figcaption>Fig. 4.7: A relationship with an attribute</figcaption>
</figure>

#### <span id="e4.3.1">Exercise 4.3.1</span>

For your E/R diagrams of

<ol style="list-style-type: lower-alpha;">
<li>[Exercise 4.1.1](#e4.1.1)</li>
<li value=3>[Exercise 4.1.6](#e4.1.6)</li>
</ol>
<ol style="list-style-type: lower-roman">
<li>Select and specify keys, and</li>
<li>Indicate appropriate referential integrity constraints.</li>
</ol>

#### <span id="e4.4.1">Exercise 4.4.1</span>

One way to represent students and the grades they get in courses is to
use entity sets corresponding to students, to courses, and to
"enrollments." Enrollment entities form a "connecting" entity set
between stuents and courses can be used to represent not only the fact
that a student is taking a certain course, but the grade of the
student in the course. Draw an E/R diagram for this situation,
indicating weak entity sets and the keys for the entity sets. Is the
grade part of the key for enrollments?

#### <span id="e4.5.1">Exercise 4.5.1</span>

Convert the E/R diagram of [Fig. 4.29](#f4.29) to a relational
database schema

<figure id="f4.29">
```tikzpicture
\node[weak entity] (bookings) {Bookings};
\node[attribute] (row) [above left=0.1cm and 0.3cm of bookings] {row} edge (bookings);
\node[attribute] (seat) [above right=0.1cm and 0.3cm of bookings] {seat} edge (bookings);

\node[ident relationship] (toCust) [below left=0.1cm and 2cm of bookings] {toCust} edge[total] (bookings);
\node[ident relationship] (toFlt) [below right=0.1cm and 2cm of bookings] {toFlt} edge[total] (bookings);

\node[entity] (cust) [below=of toCust] {Customers} edge[{Arc Barb[scale=3.0]}-] (toCust);
\node[attribute] (phone) [right=0.2 of cust] {phone} edge (cust);
\node[attribute] (SSNo) [below left=0.2 and 0.1 of cust] {\key{SSNo}} edge (cust);
\node[attribute] (name) [below=of cust] {name} edge (cust);
\node[attribute] (addr) [below right=of cust] {addr} edge (cust);

\node[entity] (flights) [below=of toFlt] {Flights} edge[{Arc Barb[scale=3.0]}-] (toFlt);
\node[attribute] (number) [below left=0.2 and 0.1 of flights] {\key{number}} edge (flights);
\node[attribute] (day) [below=of flights] {\key{day}} edge (flights);
\node[attribute] (aircraft) [below right=of flights] {aircraft} edge (flights);
```
<figcaption>Fig. 4.29: An E/R diagram about airlines</figcaption>
</figure>

#### <span id="e6.3.1">Exercise 6.3.1</span>

Write the following queries, based on the database schema:

```
Product(maker, model, type)
PC(model, speed, ram, hd, price)
Laptop(model, speed, ram, hd, screen, price)
Printer(model, color, type, price)
```

of [Exercise 2.4.1](HW2.html). You should use at least one subquery in
each of your answers and write each query in two significantly
different ways (e.g. using different sets of the operators `EXISTS`,
`IN`, `ALL`, and `ANY`).

<ol style="list-style-type: lower-roman">
<li>Find the makers of PC's with a speed of at least 3.0.</li>
<li>Find the printers with the highest price.</li>
<li class=difficult>Find the model number of the item (PC, laptop, or
printer) with the highest price.</li>
<li class=difficult>Find the maker of the color printer with the
lowest price.</li>
<li class="extra-difficult">Find the maker(s) of the PC(s) with the
fastest processor among all those PC's that have the smallest amount
of RAM</li>
</ol>
