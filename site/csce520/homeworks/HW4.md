---
title: 'Homework 4'
due: 'Due: October 22, 2019'
author: 'Dr. Fenner'
date: '2019-10-01'
time: 1569974440
tocbot: true
layout: 'post'
---

### Reading

* &#167; 3.1
* &#167; 3.2
* &#167; 3.3

### To Submit

#### Written

<dl>
<dt>**Required**</dt>
<dd>
<ul>
<li>Exercise 3.1.1</li>
<li>Exercise 3.2.1</li>
<li>Exercise 3.2.2(ii)</li>
<li>Exercise 3.2.4</li>
<li>Exercise 3.2.10(a,c)</li>
<li>Exercise 3.3.1(b,d)</li>
<li>Exercise 3.3.4</li>
</ul>
</dd>

<dt>**Graduate Students Only**</dt>
<dd>
<ul>
<li>Exercise 3.2.3(a,b)</li>
<li>Exercise 3.2.10(b,d)</li>
<li>Exercise 3.3.1(a,c)</li>
</ul>
</dd>
</dl>

#### SQL

Recall the database schema of [Exercise 2.4.1](HW2.html#e2.4.1). Writ
eSQL queries for the following things:

1. The number of PC's made by each maker.
2. The maximum screen size of a laptop made by E.
3. The average price of products made by each maker (but only if that
   average price is at least 200), in descending order of price.

In each case, give your column headings reasonably descriptive names.
Your queries must be correct in general, that is, they must work for
any instance of the DB, not just the one presented in the book. Place
your queries in a file `hw4.sql` and test them in MySQL.

The following applies to this and mutatis mutandis all future
homeworks. To submit: From the shell prompt, login to mysql with the
following shell command:

```sh
mysql -p sales
```

and enter your user password when prompted (this will work on
unix/linux/Mac systems; for Windows, YMMV). Including the database
name on the command line automatically loads it, so you do not need to
issue a "USE sales" command inside mysql. Inside mysql, type

```sql
tee hw4.log
source hw4.sql
notee
quit
```

The `tee` command tells mysql to append everything subsequently
appearing in the session to the file `hw4.log`, until the `notee`
command is given, which stops logging. The `source` command reads the
`hw4.sql` file and runs its command in order as if you had typed them
at the prompt.

You should make a .zip file `hw4.zip` containing both `hw4.sql` and
`hw4.log` files. Upload your .zip file electronically using [CSE
Dropbox](https://dropbox.cse.sc.edu).  If you are in Sections 001,
H10, or J60, you should submit your assignment to Section 001
regardless of which of these sections you are in. If you are in
Section 002, then submit to Section 002.

---

### Reference
