---
title: 'Homework 2'
due: 'Due: September 24, 2019'
author: 'Dr. Fenner'
date: '2019-10-01'
time: 1569974440
tocbot: true
layout: 'post'
---

### Reading

Read through the rest of Chapter 2

### To submit

The purpose of this assignment is to get you to do some basic SQL

#### Written

<dl>
<dt>**Required**</dt>
<dd>
<ul>
<li>[Exercise 2.2.1](#e2.2.1)</li>
<li>[Exercise 2.2.2](#e2.2.2)</li>
<li>[Exercise 2.3.1(a-f)](#e2.3.1)</li>
<li>[Exercise 2.4.1(a-e)](#e2.4.1)</li>
</ul>
</dd>
</dl>

**NOTE**: Hardcopy is due on the podium in the classroom at or before
the beginning of class. Any hardcopy turned in after that time ---
**even by a few minutes** --- will be counted as late. **This is a
firm deadline**.

#### SQL

Do [Exercise 2.4.1(a-e)](#e2.4.1) in SQL.

Place all your files in a single directory `hw2`. Create a zip of this
directory: in the parent directory, type

```bash
    zip -r hw2 hw2
```

Upload the created file `hw2.zip` to Dropbox. Note that you are not
required to include output in your submission. You should check for
yourself that everything runs correctly before you submit.

### Reference

#### <span id=e2.2.1>Exercise 2.2.1</span>

The relation `Accounts`:

| acctNo |   type   | balance |
|--------|----------|---------|
|  12345 | savings  |   12000 |
|  23456 | checking |    1000 |
|  34567 | savings  |      25 |

The relation `Customers`:

| firstName | lastName |   idNo  | account |
|-----------|----------|---------|---------|
| Robbie    | Banks    | 901-222 |   12345 |
| Lena      | Hand     | 805-333 |   12345 |
| Lena      | Hand     | 805-333 |   23456 |

In Fig 2.6 are instances of two relations that might constitute part
of a banking database. Indicate the following:

a. The attributes of each relation.
b. The tuples of each relation.
c. The components of one tuple from each relation
d. The relation schema for each relation
e. The database schema
f. A suitable domain for each attribute.
g. Another equivalent way to present each relation.

#### <span id=e2.2.2>Exercise 2.2.2</span>

In Section 2.2.7 we suggested that there are many examples of
attributes that are created for the purpose of serving as keys of
relations. Give some additional examples.

#### <span id=2.3.1>Exercise 2.3.1</span>
In this exercise we introduce one of our running examples of a
relational database schema. The database schema consists of four
relations whose schemas are:

```
    Product(maker, model, type)
    PC(model, speed, ram, hd, price)
    Laptop(model, speed, ram, hd, screen, price)
    Printer(model, color, type, price)
```

The `Product` relation gives the manufacturer, model number, and type
(PC, laptop, or printer) of various products. We assume for
convenience that model numbers are unique over all manufacturers and
product types; that assumption is not realistic, and a real database
would include a code for the manufacturer as part of the model number.
The `PC` relation gives for each model number that is a PC the speed
(of the processor, in gigahertz), the amount of RAM (in megabytes),
the size of the hard disk (in gigabytes), and the price. The `Laptop`
relation is similar, except that the screen size (in inches) is also
included. The `Printer` relation records for each printer model
whether the printer produces color output (true, if so), the process
type (laser or ink-jet, typically), and the price.

Write the following declarations:

a. A suitable schema for relation `Product`.
b. A suitable schema for relation `PC`.
c. A suitable schema for relation `Laptop`.
d. A suitable schema for relation `Printer`.
e. An alteration to your `Printer` schema from above to delete the
   attribute `color`.
f. An alteration to your `Laptop schema` from above to add the
   attribute `od` (optical-disk type, e.g., cd or dvd). Let the
   default value for this attribute be `'none'` if the laptop does not
   have an optical disk.

#### <span id=e2.4.1>Exercise 2.4.1</span>
To populate the tables, put `INSERT INTO` commands in a file
`ex2-4-1.sql`, using the data given in Figures [2.20](#f2.20) and [2.21](#f2.21) to
populate the tables.

Render the following queries as `SELECT` statements. Place these in
text files ex-2-4-1a.sql, ex2-4-1b.sql, etc. Check that they return
the correct results.

This exercise builds upon the products schema of Exercise 2.3.1.
Recall that the database schema consists of four relations, whose
schemas are:

```
    Product(maker, model, type)
    PC(model, speed, ram, hd, price)
    Laptop(model, speed, ram, hd, screen, price)
    Printer(model, color, type, price)
```

Some sample data for the relations is shown in the figures below.
Manufacturers and model numbers have been "sanitized," but the data is
typical of products on sale at the beginning of 2007.

Write expressions of relational algebra to answer the following
queries. You may use the linear notation of Section 2.4.13 if you
wish. Show the results of your query given the data below. However,
your answer should work for arbitrary data (not just the data of these
figures).

a. What PC models have a speed of at least 3.00?
b. Which manufacturers make laptops with a hard disk of at least
   100GB?
c. Find the model number and price of all products (of any type) made
   by manufacturer *B*.
d. Find the model numbers of all color laser printers
e. Find those manufacturers that sell Laptops, but not PC's.

### Figures

#### <span id=f2.20>Figure 2.20</span>

<figure>

| maker | model |   type  |
|-------|-------|---------|
| A     | 1001  | pc      |
| A     | 1002  | pc      |
| A     | 1003  | pc      |
| A     | 2004  | laptop  |
| A     | 2005  | laptop  |
| A     | 2006  | laptop  |
| B     | 1004  | pc      |
| B     | 1005  | pc      |
| B     | 1006  | pc      |
| B     | 2007  | laptop  |
| C     | 1007  | pc      |
| D     | 1008  | pc      |
| D     | 1009  | pc      |
| D     | 1010  | pc      |
| D     | 3004  | printer |
| D     | 3005  | printer |
| E     | 1011  | pc      |
| E     | 1012  | pc      |
| E     | 1013  | pc      |
| E     | 2001  | laptop  |
| E     | 2002  | laptop  |
| E     | 2003  | laptop  |
| E     | 3001  | printer |
| E     | 3002  | printer |
| E     | 3003  | printer |
| F     | 2008  | laptop  |
| F     | 2009  | laptop  |
| G     | 2010  | laptop  |
| H     | 3006  | printer |
| H     | 3007  | printer |

<figcaption>Sample data for `Product`</figcaption>
</figure>


#### <span id=f2.21>Figure 2.21</span>

<figure>

<figure>

| model | speed |  ram |  hd | price |
|-------|-------|------|-----|-------|
| 1001  |  2.66 | 1024 | 250 |  2114 |
| 1002  |  2.10 |  512 | 250 |   995 |
| 1003  |  1.42 |  512 |  80 |   478 |
| 1004  |  2.80 | 1024 | 250 |   649 |
| 1005  |  3.20 |  512 | 250 |   630 |
| 1006  |  3.20 | 1024 | 320 |  1049 |
| 1007  |  2.20 | 1024 | 200 |   510 |
| 1008  |  2.20 | 2048 | 250 |   770 |
| 1009  |  2.00 | 1024 | 250 |   650 |
| 1010  |  2.80 | 2048 | 300 |   770 |
| 1011  |  1.86 | 2048 | 160 |   959 |
| 1012  |  2.80 | 1024 | 160 |   649 |
| 1013  |  3.06 |  512 |  80 |   529 |

<figcaption>(a) Sample data for relation `PC`</figcaption>
</figure>

<figure>

| model | speed |  ram |  hd | screen | price |
|-------|-------|------|-----|--------|-------|
|  2001 |  2.00 | 2048 | 240 |   20.1 |  3673 |
|  2002 |  1.73 | 1024 |  80 |   17.0 |   949 |
|  2003 |  1.80 |  512 |  60 |   15.4 |   549 |
|  2004 |  2.00 |  512 |  60 |   13.3 |  1150 |
|  2005 |  2.16 | 1024 | 120 |   17.0 |  2500 |
|  2006 |  2.00 | 2048 |  80 |   15.4 |  1700 |
|  2007 |  1.83 | 1024 | 120 |   13.3 |  1429 |
|  2008 |  1.60 | 1024 | 100 |   15.4 |   900 |
|  2009 |  1.60 |  512 |  80 |   14.1 |   680 |
|  2010 |  2.00 | 2048 | 160 |   15.4 |  2300 |

<figcaption>(b) Sample data for relation `Laptop`</figcaption>
</figure>

<figure>

| model | color |    type   | price |
|-------|-------|-----------|-------|
|  3001 | true  | 'ink-jet' |    99 |
|  3002 | false | 'laser'   |   239 |
|  3003 | true  | 'laser'   |   899 |
|  3004 | true  | 'ink-jet' |   120 |
|  3005 | false | 'laser'   |   120 |
|  3006 | true  | 'ink-jet' |   100 |
|  3007 | true  | 'laser'   |   200 |

<figcaption>(c) Sample data for relation `Printer`</figcaption>
</figure>
<figcaption>Sample data for relations of [Exercise 2.4.1](#e2.4.1)</figcaption>
</figure>
