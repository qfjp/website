---
title: 'Homework 6'
due: 'Due: December 3, 2019'
author: 'Dr. Fenner'
date: '2019-10-01'
time: 1569974440
tocbot: true
layout: 'post'
---

### To Submit

#### Written

<dl>
<dt>**Required**</dt>
<dd>
<ul>
<li>[Exercise 5.3.1(a,b,c)](#e5.3.1)</li>
<li>[Exercise 6.6.1(a,b)](#e6.6.1)</li>
<li>[Exercise 11.1.2](#e11.1.2)</li>
<li>[Exercise 11.2.2](#e11.2.2)</li>
<li>[Exercise 11.3.1(a,b)](#e11.3.1)</li>
<li>[Exercise 11.3.2](#e11.3.2)</li>
<li>[Exercise 12.1.2(b,d)](#e12.1.2)</li>
<li>[Exercise 12.2.1(b)](#e12.2.1)</li>
</ul>
</dd>

<dt>**Graduate Students Only**</dt>
<dd>
<ul>
<li>[Exercise 5.3.1(d,e)](#e5.3.1)</li>
<li>[Exercise 6.6.1(c,d)](#e6.6.1)</li>
</ul>
</dd>
</dl>

**NOTE:** Exercise 6.6.1 is *not* a MySQL exercise.  Use the flavor of
SQL given in the book and submit your hand-written answer with the
others at or before the beginning of class or else electronically
before midnight. Each of your answers should be one or more SQL
statements surrounded by high-level pseudocode. Be sure to group your
SQL statements and queries into transactions as appropriate, as if
there were others who may be querying/updating the database
concurrently.

---

### Reference

#### <span id=e5.3.1>Exercise 5.3.1</span>

Write each of Exercise 2.4.1 (reproduced below) in Datalog. You should
use only safe rules, but you may wish to use several IDB predicates
corresponding to subexpressions of complicated relational-algebra
expressions.

Schema:

```
Product(maker, model, type)
PC(model, speed, ram, hd, price)
Laptop(model, speed, ram, hd, screen, price)
Printer(model, color, type, price)
```

Queries:

<ol style="list-style-type: lower-alpha">
<li>What PC models have a speed of at least 3.00?</li>
<li>Which manufacturers make laptops with a hard disk of at least
100GB?</li>
<li>Find the model number and price of all products (of any type) made
by manufacturer B.</li>
<li>Find the model numbers of all color laser printers?</li>
<li>Find those manufacturers that sell Laptops, but not PC's.</li>
</ol>

#### <span id=e6.6.1>Exercise 6.6.1</span>

This exercise involves certain programs that operate on the two
relations

```
Product(maker, model, type)
PC(model, speed, ram, hd, price)
```

from our running PC exercise. Sketch the following programs, including
SQL statements and work done in a conventional language. Do not forget
to issue `BEGIN TRANSACTION`, `COMMIT`, and `ROLLBACK` statements at
the proper times and to tell the system your transactions are
read-only if they are.

<ol style="list-style-type: lower-alpha">
<li>Given a speed and amount of RAM (as arguments of the function),
look up the PC's with that speed and RAM, printing the model number
and price of each.</li>
<li>Given a model number, delete the tuple for that model from both `PC`
and `Product`.</li>
<li>Given a model number, decrease the price of that model PC by $$100.</li>
<li>Given a maker, model number, processor speed, RAM size, hard-disk
size, and price, check that there is no product with that model. If
there is such a model, print an error message for the user. If no such
model existed in the database, enter the information about that model
into the `PC` and `Product` tables.</li>
</ol>

#### <span id=e11.1.2>Exercise 11.1.2</span>

Suggest how typical data about banks and customers, as in [Exercise
4.1.1](HW5.html#e4.1.1), could be represented in the semistructured
model.

#### <span id=e11.2.2>Exercise 11.2.2</span>

Show that any relation can be represented by an XML document. *Hint:*
Create an element for each tuple with a subelement for each component
of that tuple.

#### <span id=e11.3.1>Exercise 11.3.1</span>

Add to the document of [Fig. 11.10](#f11.10) the following facts:

<ol style="list-style-type: lower-alpha;">
<li id=e11.3.1a>Carrie Fisher and Mark Hamill also starred in *The Empire Strikes
Back* (1980) and *Return of the Jedi* (1983)</li>
<li>Harrison Ford also starred in *Star Wars*, in the two movies
mentioned in [(a)](#e11.3.1a), and the movie *Firewall* (2006)</li>
</ol>

#### <span id=e11.3.2>Exercise 11.3.2</span>

Suggest how typical data about banks and customers, as was described
in [Exercise 4.1.1](HW5.html#e4.1.1), could be represented as a DTD.

#### <span id=e12.1.2>Exercise 12.1.2</span>

The document of [Fig. 12.6](#f12.6) contains data similar to that used
in our running battleships exercise. In this document, data about
ships is nested within their class element, and information about
battles appears inside each ship element. Write the following queries
in XPath. What is the result of each?

<ol style="list-style-type: lower-alpha;">
<li value=2>Find all the `Class` elements for classes with a
displacement larger than 35000.</li>
<li value=4>Find the names of the ships that were sunk.</li>
</ol>

#### <span id=e12.2.1>Exercise 12.2.1</span>

Using the product data from Figs. [12.4](#f12.4) and [12.5](#f12.5),
write the following in XQuery.

<ol style="list-style-type: lower-alpha;">
<li value=2>Find the `Printer` elements with a price less than 100,
and produce the sequence of these elements surrounded by a tag
`<CheapPrinters>`.</li>
</ol>

### Figures

#### <span id=f11.10>Figure 11.10</span>
<figure>
```xml
<? xml version = "1.0" encoding = "utf-8" standalone = "yes" ?>
<StarMovieData>
    <Star starID = "cf" starredIn = "sw">
        <Name>Carrie Fisher</Name>
        <Address>
            <Street>123 Maple St.</Street>
            <City>Hollywood</City>
        </Address>
        <Address>
            <Street>5 Locust Ln.</Street>
            <City>Malibu</City>
        </Address>
    </Star>
    <Star starID = "mh" starredIn = "sw">
        <Name>Mark Hamill</Name>
        <Address>
            <Street>456 Oak Rd.</Street>
            <City>Brentwood</City>
        </Address>
    </Star>
    <Movie movieID = "sw" starsOf = "cf mh">
        <Title>Star Wars</Title>
        <Year>1977</Year>
    </Movie>
</StarMovieData>
```
<figcaption>Adding stars-in information to our XML document</figcaption>
</figure>

#### <span id=f12.4>Figure 12.4</span>

<figure>
```xml
<Products>
    <Maker name = "A">
        <PC model = "1001" price = "2114">
            <Speed>2.66</Speed>
            <RAM>1024</RAM>
            <HardDisk>250</HardDisk>
        </PC>
        <PC model = "1002" price = "995">
            <Speed>2.10</Speed>
            <RAM>512</RAM>
            <HardDisk>250</HardDisk>
        </PC>
        <Laptop model = "2004" price = "1150">
            <Speed>2.00</Speed>
            <RAM>512</RAM>
            <HardDisk>60</HardDisk>
            <Screen>13.3</Screen>
        </Laptop>
        <Laptop model = "2005" price = "2500">
            <Speed>2.16</Speed>
            <RAM>1024</RAM>
            <HardDisk>120</HardDisk>
            <Screen>17.0</Screen>
        </Laptop>
    </Maker>
```
<figcaption>XML document with product data --- beginning</figcaption>
</figure>

#### <span id=f12.5>Figure 12.5</span>

<figure>
```xml
    <Maker name = "E">
        <PC model = "1011" price = "959">
            <Speed>1.86</Speed>
            <RAM>2048</RAM>
            <HardDisk>160</HardDisk>
        </PC>
        <PC model = "1012" price = "649">
            <Speed>2.80</Speed>
            <RAM>1024</RAM>
            <HardDisk>160</HardDisk>
        </PC>
        <Laptop model = "2001" price = "3673">
            <Speed>2.00</Speed>
            <RAM>2048</RAM>
            <HardDisk>240</HardDisk>
            <Screen>20.1</Screen>
        </Laptop>
        <Printer model = "3002" price = "239">
            <Color>false</Color>
            <Type>laser</Type>
        </Printer>
    </Maker> <!-- Missing from original figure -->
    <Maker name = "H">
        <Printer model = "3006" price = "100">
            <Color>true</Color>
            <Type>ink-jet</Type>
        </Printer>
        <Printer model = "3007" price = "200">
            <Color>true</Color>
            <Type>laser</Type>
        </Printer>
    </Maker>
</Products>
```
<figcaption>XML document with product data --- end</figcaption>
</figure>

#### <span id=f12.6>Figure 12.6</span>
<figure>
```xml
<Ships>
    <Class name = "Kongo" type = "bc" country = "Japan"
            numGuns = "8" bore = "14" displacement = "32000">
        <Ship name = "Kongo" launched = "1913" />
        <Ship name = "Hiei" launched = "1914" />
        <Ship name = "Kirishima" launched = "1915">
            <Battle outcome = "sunk">Gaudalcanal</Battle>
        </Ship>
        <Ship name = "Haruna" launched = "1915">
    </Class>
    <Class name = "North Carolina" type = "bb" country = "USA"
            numGuns = "9" bore = "16" displacement = "37000">
        <Ship name = "North Carolina" launched = "1941" />
        <Ship name = "Washington" launched = "1941">
            <Battle outcome = "ok">Gaudalcanal</Battle>
        </Ship>
    </Class>
    <Class name = "Tennessee" type = "bb" country = "USA"
            numGuns = "12" bore = "14" displacement = "32000">
        <Ship name = "Tennessee" launched = "1920">
            <Battle outcome = "ok">Surigao Strait</Battle>
        </Ship>
        <Ship name = "California" launched = "1921">
            <Battle outcome = "ok">Surigao Strait</Battle>
        </Ship> <!-- Missing from original figure -->
    </Class>
    <Class name = "King George V" type = "bb"
            country = "Great Britain"
            numGuns = "10" bore = "14" displacement = "32000">
        <Ship name = "King George V" launched = "1940" />
        <Ship name = "Prince of Wales" launched = "1941">
            <Battle outcome = "damaged">Denmark Strait</Battle>
            <Battle outcome = "sunk">Malaya</Battle>
        </Ship>
        <Ship name = "Duke of York" launched = "1941">
            <Battle outcome = "ok">North Cape</Battle>
        </Ship>
        <Ship name = "Howe" launched = "1942" />
        <Ship name = "Anson" launched = "1942" />
    </Class>
</Ships>
```
<figcaption>XML document containing battleship data</figcaption>
</figure>
