---
title: Home
---

```tikzpicture
      \draw[step=1.0, color=white, thin] (0,0) grid (6,8);

      % Finite Control
      \draw[thick] (1.5,5) rectangle (4.5, 7);
      \node at (2.4, 6.5) {$$\bs{\delta}$$};
      \node at (3, 5.5) {Finite Control};

      % Input Tape
      \draw[thick] (1, 3) rectangle (5, 4);
      \draw[thick] (2, 3) -- (2, 4);
      \draw[thick] (4, 3) -- (4, 4);
      \node at (1.5, 3.5) {$$w_0$$};
      \node at (3, 3.5) {$$\cdots$$};
      \node at (4.5, 3.5) {$$w_{n - 1}$$};
      \node at (3, 2.5) {Input Tape};

      % Connectors
      \draw[thick, ->>] (1.5, 6) .. controls (0, 6) and (1.5, 5) .. (1.5, 4.0);

      % PDA
      \draw[step=1.0, color=white, thin] (7,0) grid (13,8);
      % Finite Control
      \draw[thick] (8.5,5) rectangle (11.5, 7);
      \node at (9.4, 6.5) {$$\bs{\delta}$$};
      \node at (10, 5.5) {Finite Control};

      % Input Tape
      \draw[thick] (8, 3.5) rectangle (12, 4.5);
      \draw[thick] (9, 3.5) -- (9, 4.5);
      \draw[thick] (11, 3.5) -- (11, 4.5);
      \node at (8.5, 4) {$$s_0$$};
      \node at (10, 4) {$$\cdots$$};
      \node at (11.5, 4) {$$s_{n - 1}$$};
      \node at (10, 3) {Input Tape};

      % Stack
      \draw[thick] (8,2) -- (12,2) -- (12, 1) -- (8, 1);
      \draw[thick] (11, 1) -- (11, 2);
      \draw[thick] (10, 1) -- (10, 2);
      \node at (11.5, 1.5) {$$Z_0$$};
      \node at (10.5, 1.5) {$$X$$};
      \node at (9, 1.5) {$$\cdots$$};
      \node at (10, 0.5) {Stack};

      % Connectors
      \draw[thick, ->>] (8.5, 6) .. controls (7, 6) and (8.5, 5) .. (8.5, 4.5);
      \draw[thick, -] (11.5, 6) .. controls (12, 6) and (13, 5) .. (13, 4);
      \draw[thick, ->>] (13, 4) .. controls (13, 2) and (9.5, 3.3) .. (9.5, 2);
```

## Homework

The most recent homeworks[^testfootnote]:

$partial("templates/homework-list.html")$

[^testfootnote]: Mostly LaTeX tests
