--------------------------------------------------------------------------------
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections     #-}

module Main where

import           Data.Monoid                (mappend)
import           Debug.Trace
import           Hakyll
import           Text.Pandoc.Options        (Extension (..),
                                             HTMLMathMethod (MathJax),
                                             ReaderOptions (..),
                                             WriterOptions (..),
                                             extensionsFromList,
                                             readerExtensions,
                                             writerHTMLMathMethod)

import           Hakyll.Core.Item           (withItemBody)

import           Filesystem.Path            (dirname, filename,
                                             replaceExtension)
import           Filesystem.Path.CurrentOS  (decodeString, encodeString)
import           System.Process.Typed       (ProcessConfig, checkExitCode, proc,
                                             withProcess)

import           Control.Monad              (join)
import           Data.List                  (group, sort)
import           System.Directory           (copyFile, listDirectory)
import           System.FilePath.Glob       (glob)

import qualified Data.Text                  as T (null, pack, strip, unpack)
import           Text.HTML.TagSoup          (Tag (TagText), fromTagText,
                                             isTagText)

import           Control.Monad              ((>=>))
import           Data.ByteString.Lazy.Char8 (pack, unpack)
import qualified Network.URI.Encode         as URI (encode)
import           Text.Pandoc.Definition
import           Text.Pandoc.Walk           (walkM)

import           Codepoints                 (escapeXML)

--------------------------------------------------------------------------------
preconvertHomework :: IO ()
preconvertHomework = do
    texFiles <- glob "csce355/homeworks/*tex"
    dirs <- mapM tex2htmlProc texFiles
    let noDups = map head . group . sort $ dirs
    contents <- join <$> mapM listDirectory noDups
    let copyFn path =
            let b = filename (decodeString path)
             in trace path $
                copyFile path ("csce355/homeworks/" ++ (encodeString b))
    mapM_ copyFn contents

preconvertTexFile :: String -> IO ()
preconvertTexFile file = do
    compiledDir <- tex2htmlProc file
    contents <- listDirectory compiledDir
    let destDir = encodeString . dirname . decodeString $ file
    let copyFn path =
            let b = filename (decodeString path)
             in trace path $ copyFile path (destDir ++ "/" ++ encodeString b)
    mapM_ copyFn contents

routeTexFileNFriends :: String -> Identifier -> Rules ()
routeTexFileNFriends name template = do
    match (fromGlob $ name ++ ".tex") $
        version "raw" $ do
            route idRoute
            compile copyFileCompiler
    match (fromGlob $ name ++ ".html") $ do
        route $ setExtension "html"
        compile $
            texCompiler >>= loadAndApplyTemplate template defaultContext >>=
            loadAndApplyTemplate "templates/main-html.html" defaultContext >>=
            relativizeUrls
    match (fromGlob $ name ++ ".css") $ do
        route idRoute
        compile compressCssCompiler
    match (fromGlob $ name ++ ".tex") $
        version "pdf" $ do
            route $ setExtension "pdf"
            compile $
                getResourceLBS >>=
                withItemBody (unixFilterLBS "rubber-pipe" ["-d"])
    match (fromGlob $ name ++ "*svg") $ do
        route idRoute
        compile copyFileCompiler

main :: IO ()
main = do
    preconvertHomework
    preconvertTexFile "csce355/Syllabus.tex"
    preconvertTexFile "csce355/Project.tex"
    preconvertTexFile "papers/rc-cross.tex"
    hakyll $ do
        routeTexFileNFriends "papers/rc-cross" "templates/default.html"
        routeTexFileNFriends "csce355/Syllabus" "templates/csce355.html"
        routeTexFileNFriends "csce355/Project" "templates/csce355.html"
        match (fromList ["DanielPadeResume.pdf", "DanielPadeCV.pdf"]) $ do
            route idRoute
            compile copyFileCompiler
        match
            (fromList
                 [ "papers/inglis2012.pdf"
                 , "papers/PVNumbers.pdf"
                 , "papers/detector.png"
                 ]) $ do
            route idRoute
            compile copyFileCompiler
        match "images/*" $ do
            route idRoute
            compile copyFileCompiler
        match "css/*.css" $ do
            route idRoute
            compile compressCssCompiler
        match "css/*.scss" $ do
            route $ setExtension "css"
            compile compressScssCompiler
        match "js/*.js" $ do
            route idRoute
            compile copyFileCompiler
        match
            (fromList
                 [ "about.rst"
                 , "contact.markdown"
                 , "teaching.markdown"
                 , "research.markdown"
                 ]) $ do
            route $ setExtension "html"
            compile $
                pandocCompiler >>=
                loadAndApplyTemplate "templates/default.html" defaultContext >>=
                loadAndApplyTemplate "templates/main-html.html" defaultContext >>=
                relativizeUrls
        match "posts/*" $ do
            route $ setExtension "html"
            compile $
                pandocCompiler >>=
                loadAndApplyTemplate "templates/post.html" postCtx >>=
                loadAndApplyTemplate "templates/default.html" postCtx >>=
                loadAndApplyTemplate "templates/main-html.html" postCtx >>=
                relativizeUrls
        match "index.markdown" $ do
            route $ setExtension "html"
            compile $ do
                posts <- recentFirst =<< loadAll "posts/*"
                let indexCtx =
                        listField "posts" postCtx (return posts) `mappend`
                        constField "title" "" `mappend`
                        defaultContext
                writePandocWith markdownWriteOpts <$>
                    (getResourceBody >>= applyAsTemplate indexCtx >>=
                     readPandocWith markdownReadOpts >>=
                     traverse (walkM tikzFilter)) >>=
                    loadAndApplyTemplate "templates/default.html" indexCtx >>=
                    loadAndApplyTemplate "templates/main-html.html" indexCtx >>=
                    relativizeUrls
        match "csce520/homeworks/*.md" $ do
            route $ setExtension "html"
            compile $
                writePandoc <$>
                (getResourceBody >>= applyAsTemplate postCtx >>= readPandoc >>=
                 traverse (walkM tikzFilter)) >>=
                loadAndApplyTemplate "templates/homework.html" postCtx >>=
                loadAndApplyTemplate "templates/csce520.html" postCtx >>=
                loadAndApplyTemplate "templates/main-html.html" postCtx >>=
                relativizeUrls
        match "csce355/homeworks/*tex" $
            version "raw" $ do
                route idRoute
                compile copyFileCompiler
        match "csce355/homeworks/*tex" $
            version "pdf" $ do
                route $ setExtension "pdf"
                compile $
                    getResourceLBS >>=
                    withItemBody (unixFilterLBS "rubber-pipe" ["-d"])
        match "csce355/homeworks/*html" $ do
            route $ setExtension "html"
            compile $
                texCompiler >>=
                loadAndApplyTemplate "templates/homework.html" homeworkCtx >>=
                loadAndApplyTemplate "templates/csce355.html" homeworkCtx >>=
                loadAndApplyTemplate "templates/main-html.html" homeworkCtx >>=
                relativizeUrls
        match "csce355/homeworks/*css" $ do
            route idRoute
            compile compressCssCompiler
        match "csce355/homeworks/*svg" $ do
            route idRoute
            compile $ getResourceBody >>= escapeXmlCompiler
        create ["csce355/homeworks.html"] $ do
            route idRoute
            compile $ do
                homeworks <- recentFirst =<< loadAll "csce355/homeworks/*html"
                let archiveCtx =
                        listField "homeworks" homeworkCtx (return homeworks) `mappend`
                        constField "title" "Homework" `mappend`
                        defaultContext
                makeItem "" >>=
                    loadAndApplyTemplate "templates/homeworks.html" archiveCtx >>=
                    loadAndApplyTemplate "templates/csce355.html" archiveCtx >>=
                    loadAndApplyTemplate "templates/main-html.html" archiveCtx >>=
                    relativizeUrls
        create ["csce520/homeworks.html"] $ do
            route idRoute
            compile $ do
                homeworks <- recentFirst =<< loadAll "csce520/homeworks/*"
                let archiveCtx =
                        listField "homeworks" homeworkCtx (return homeworks) `mappend`
                        constField "title" "Homework" `mappend`
                        defaultContext
                makeItem "" >>=
                    loadAndApplyTemplate "templates/homeworks.html" archiveCtx >>=
                    loadAndApplyTemplate "templates/csce520.html" archiveCtx >>=
                    loadAndApplyTemplate "templates/main-html.html" archiveCtx >>=
                    relativizeUrls
        match "csce355/index.markdown" $ do
            route $ setExtension "html"
            compile $ do
                homeworks <- recentFirst =<< loadAll "csce355/homeworks/*html"
                let indexCtx =
                        listField "homeworks" homeworkCtx (return homeworks) `mappend`
                        constField "title" "Foundations of Computation" `mappend`
                        defaultContext
                writePandocWith markdownWriteOpts <$>
                    (getResourceBody >>= applyAsTemplate indexCtx >>=
                     readPandocWith markdownReadOpts >>=
                     traverse (walkM tikzFilter)) >>=
                    loadAndApplyTemplate "templates/csce355.html" indexCtx >>=
                    loadAndApplyTemplate "templates/main-html.html" indexCtx >>=
                    relativizeUrls
        match "csce520/index.markdown" $ do
            route $ setExtension "html"
            compile $ do
                homeworks <- recentFirst =<< loadAll "csce520/homeworks/*"
                let indexCtx =
                        listField "homeworks" homeworkCtx (return homeworks) `mappend`
                        constField "title" "Database System Design" `mappend`
                        defaultContext
                writePandocWith markdownWriteOpts <$>
                    (getResourceBody >>= applyAsTemplate indexCtx >>=
                     readPandocWith markdownReadOpts >>=
                     traverse (walkM tikzFilter)) >>=
                    loadAndApplyTemplate "templates/csce520.html" indexCtx >>=
                    loadAndApplyTemplate "templates/main-html.html" indexCtx >>=
                    relativizeUrls
        match "templates/*" $ compile templateBodyCompiler

--------------------------------------------------------------------------------
escapeTag :: Tag String -> Tag String
escapeTag tag =
    if isTagText tag
        then TagText . escapeXML . fromTagText $ tag
        else tag

escapeXmlCompiler :: Item String -> Compiler (Item String)
escapeXmlCompiler item = return $ fmap (withTags escapeTag) item

-- | Takes a tex file and returns a directory of compiled contents
tex2htmlProc :: FilePath -> IO FilePath
tex2htmlProc fname = do
    let compilation = createProc fname
    withProcess compilation (\p -> checkExitCode p)
    let newFname =
            encodeString .
            filename . (flip replaceExtension) "ss" . decodeString $
            fname
    return newFname

createProc :: FilePath -> ProcessConfig () () ()
createProc fname =
    proc
        "make4ht"
        [ "-uf"
        , "html5+tidy+staticsite"
        , "-e"
        , "./tex2html/new.mk4"
        , "-c"
        , "./tex2html/nma_mathjax.cfg"
        , fname
        , "nma,fn-in,notoc*,p-width,charset=utf-8,picalign"
        , " -cunihtf -utf8"
        ]

getFilePath :: Compiler (Item String)
getFilePath = getResourceFilePath >>= makeItem

postCtx :: Context String
postCtx = dateField "date" "%B %e, %Y" `mappend` defaultContext

homeworkCtx :: Context String
homeworkCtx = defaultContext

markdownWriteOpts :: WriterOptions
markdownWriteOpts = defaultHakyllWriterOptions

markdownReadOpts :: ReaderOptions
markdownReadOpts =
    defaultHakyllReaderOptions
        { readerExtensions =
              extensionsFromList
                  [ Ext_backtick_code_blocks
                  , Ext_fenced_code_attributes
                  , Ext_fenced_code_blocks
                  , Ext_fenced_divs
                  , Ext_pipe_tables
                  , Ext_multiline_tables
                  , Ext_raw_html
                  , Ext_styles
                  , Ext_footnotes
                  ]
        }

renderMarkdown :: Item String -> Compiler (Item String)
renderMarkdown = renderPandocWith markdownReadOpts markdownWriteOpts

markdownCompiler :: Compiler (Item String)
markdownCompiler =
    pandocCompilerWithTransformM markdownReadOpts markdownWriteOpts $
    walkM tikzFilter

texReadOpts :: ReaderOptions
texReadOpts =
    defaultHakyllReaderOptions
        { readerStandalone = True
        , readerExtensions =
              extensionsFromList
                  [ Ext_latex_macros
                  , Ext_raw_tex
                  , Ext_native_divs
                  , Ext_link_attributes
                  , Ext_raw_html
                  , Ext_shortcut_reference_links
                  , Ext_citations
                  ]
        }

texWriteOpts :: WriterOptions
texWriteOpts =
    defaultHakyllWriterOptions
        { writerHTMLMathMethod = MathJax ""
        , writerExtensions =
              extensionsFromList
                  [Ext_native_divs, Ext_link_attributes, Ext_raw_html]
        }

texCompiler :: Compiler (Item String)
texCompiler = pandocCompilerWith texReadOpts texWriteOpts

tikzFilter :: Block -> Compiler Block
tikzFilter (CodeBlock (id, "tikzpicture":extraClasses, namevals) contents) =
    (imageBlock .
     ("data:image/svg+xml;utf8," ++) . URI.encode . filter (/= '\n') . itemBody <$>) $
    makeItem contents >>=
    loadAndApplyTemplate (fromFilePath "templates/tikz.tex") (bodyField "body") >>=
    withItemBody
        (return . pack >=>
         unixFilterLBS "rubber-pipe" ["--pdf"] >=>
         unixFilterLBS "pdftocairo" ["-svg", "-", "-"] >=> return . unpack)
  where
    imageBlock fname =
        Para [Image (id, "tikzpicture" : extraClasses, namevals) [] (fname, "")]
tikzFilter x = return x

-- | Create a SCSS compiler that transpiles the SCSS to CSS and
-- minifies it (relying on the external 'sass' tool)
compressScssCompiler :: Compiler (Item String)
compressScssCompiler = do
    fmap (fmap compressCss) $
        getResourceString >>=
        withItemBody
            (unixFilter
                 "sass"
                 [ "-s"
                 , "--scss"
                 , "--style"
                 , "compressed"
                 , "--load-path"
                 , "scss"
                 ])
