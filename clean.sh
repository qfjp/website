#/bin/sh
rm site/*.4tc
rm site/*.4ct
rm site/*.log
rm site/*.lg
rm site/*.aux
rm site/*.tmp
rm site/*.xref
rm site/*.idv
rm site/*.dvi
rm site/*.svg
rm -r site/*.ss

rm site/csce355/*.css
rm site/csce355/*.html
rm site/csce355/*.published

rm site/csce355/homeworks/*.svg
rm site/csce355/homeworks/*.css
rm site/csce355/homeworks/*.html
rm site/csce355/homeworks/*.published

rm site/papers/*.svg
rm site/papers/*.published
rm site/papers/*.html
rm site/papers/*.css

rm site/*.css
rm site/*.html
